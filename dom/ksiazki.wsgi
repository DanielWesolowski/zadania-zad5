from itertools import starmap

__author__ = 'Daniel'
#!/usr/bin/env python
# -*- coding=utf-8 -*-

import datetime
import bookdb


body1 = """
<html>
    <head>
        <meta charset="utf-8" />
        <title>Zad1: Informacje CGI</title>
    </head>
    <body>
        %s <br/>
    """
body2 = """
    </body>
</html>
"""


def application(environ, start_response):

    licznik = 0
    status = '200 OK'

    pytanie = environ['QUERY_STRING']

    spisKsiazekId = bookdb.BookDB().titles()
    tablicaID = []
    for item in spisKsiazekId:
        tablicaID.append(item['id'])

    if pytanie:
        tmp = str(pytanie)
        if pytanie in tablicaID:
            response_body = body1 % ("Opis ksiazki:")
            opisKsiazki = bookdb.BookDB().title_info(environ['QUERY_STRING'])
            for item in opisKsiazki:
                tmp = item + "   " + opisKsiazki[item] + "<br/>"
                response_body += tmp
            status = '200 OK'
        else:
            response_body = body1 % ("404 File not found")
            status = '404 File not found'
    else:
        response_body = body1 % ("Spis ksiazek w bazie danych:")
        for item in bookdb.BookDB().titles():
            licznik += 1
            link = "<a href='?" + item['id'] + "'>" + str(licznik)+"    " + item['title'] + "</a><br/>"
            response_body += link
        status = '200 OK'

    response_body += body2


    response_headers = [('Content-Type', 'text/html'),
                        ('Content-Length', str(len(response_body)))]
    start_response(status, response_headers)

    return [response_body]


if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    srv = make_server('localhost', 5432, application)
    srv.serve_forever()