__author__ = 'Daniel'
import unittest
import httplib

def zawartosc_strony(path="/~p11/wsgi"):
        polaczenie = httplib.HTTPConnection('194.29.175.240', '80')
        polaczenie.request("GET", path)
        return polaczenie.getresponse().status


class TestyKsiazek(unittest.TestCase):

    def test_strona_glowna(self):
        zawartowsc = zawartosc_strony("")
        status = 200
        self.assertEqual(zawartowsc, status)

    def test_id1(self):
        zawartowsc = zawartosc_strony("?id1")
        status = 200
        self.assertEqual(zawartowsc, status)

    def test_id2(self):
        zawartowsc = zawartosc_strony("?id2")
        status = 200
        self.assertEqual(zawartowsc, status)

    def test_id3(self):
        zawartowsc = zawartosc_strony("?id3")
        status = 200
        self.assertEqual(zawartowsc, status)

    def test_id4(self):
        zawartowsc = zawartosc_strony("?id4")
        status = 200
        self.assertEqual(zawartowsc, status)

    def test_id5(self):
        zawartowsc = zawartosc_strony("?id5")
        status = 200
        self.assertEqual(zawartowsc, status)

if __name__ == '__main__':
    unittest.main()