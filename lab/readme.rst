
Laboratorium #5
===============

1. Wypełnić brakujące dane odpowiednimi zmiennymi środowiskowymi w pliku ``cgi_info.cgi`` i uruchomić
   jako skrypt CGI na serwerze laboratoryjnym.

2. To samo, co w pkt. 1 tylko w pliku ``info.wsgi`` i uruchomić jako skrypt WSGI na serwerze laboratoryjnym.
   Ponadto zastosować 2 middleware:

    * transformujący wyjście do postaci wielkich liter,
    * przekazujący do aplikacji informację o czasie ostatniej wizyty danego (konkretnego a nie dowolnego)
      użytkownika na stronie.

   Aplikacja powinna działać poprawnie zarówno z włączonymi jak i wyłączonymi warstwami pośrednimi.
